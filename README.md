# Funny Movies Server

## Technologies

- Environment: NodeJS
- Language: TypeScript
- Framework: ExpressJS
- ORM: Prisma
- Database: PostgreSQL
- Authentication: Basic, JWT
- Password hash library: bcryptjs

## Prequisites

- Create the `.env` file by copying the `.env.example` file
- Database: 
  - An easy way to have a PostgreSQL server is using Docker or using PostgreSQL Cloud (or Render).
  - Once you have the database connection URL, replace the sample one in the `.env` file by yours.
- Install dependencies:

```bash
yarn
```

- Initialize database schema:

```bash
npx prisma migrate dev --name "init" --preview-feature
```

## NPM scripts

1. Run dev:

```bash
yarn dev
```

2. Lint:

```bash
yarn lint
```

3. Build: 

```bash
yarn build
```

4. Run production:

```bash
yarn start
```

5. Run tests:

```bash
yarn test
```

## Other useful scripts

1. Re-generate types from the schema:

```bash
npx prisma generate
```

2. Migrate database based on the schema (DO NOT RUN THIS IN PRODUCTION ENVIRONMENT):

```bash
npx prisma migrate dev --name "init-staging" --preview-feature
```

3. Introspects the database to infer and executes the changes required to make your database schema reflect the state of the Prisma schema:

```bash
npx prisma db push
```
