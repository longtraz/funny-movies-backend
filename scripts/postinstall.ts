import { exec } from "child_process";
run();

function run() {
  if (process.env.NODE_ENV !== "production") return;

  exec("prisma db push --accept-data-loss", function (error, stdout, stderr) {
    console.log("stdout: " + stdout);
    console.log("stderr: " + stderr);
    if (error !== null) {
      console.log("exec error: " + error);
    }
  });
}
