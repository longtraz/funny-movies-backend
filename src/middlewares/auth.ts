import httpStatus from "http-status";
import { NextFunction, Request, Response } from "express";

import { ApiError } from "../errors/ApiError";
import { authService } from "../services";

export const authenticate = async (req: Request, res: Response, next: NextFunction) => {
  const authorizationHeader = req.get("Authorization");

  if (!authorizationHeader) return next(new ApiError(httpStatus.UNAUTHORIZED, "You are not authenticated"));

  const jwt = authorizationHeader.replace("Bearer ", "");

  const authenticatedUser = await authService.verifyAuthToken(jwt);

  if (!authenticatedUser) return next(new ApiError(httpStatus.UNAUTHORIZED, "You are not authenticated"));

  req.requester = authenticatedUser;

  next();
};
