import express from "express";

import { userController } from "../controllers";
import validate from "../middlewares/validate";
import { userValidation } from "../validations";

const router = express.Router();

router.route("/").post(validate(userValidation.createUser), userController.createUser);

router.route("/:userId").get(validate(userValidation.getUser), userController.getUser);

export { router as userRoute };
