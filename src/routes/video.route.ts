import express from "express";

import { videoController } from "../controllers";
import validate from "../middlewares/validate";
import { authenticate } from "../middlewares/auth";
import { videoValidation } from "../validations";

const router = express.Router();

router
  .route("/")
  .get(authenticate, validate(videoValidation.listVideos), videoController.listVideos)
  .post(authenticate, validate(videoValidation.createVideo), videoController.createVideo);

export { router as videoRoute };
