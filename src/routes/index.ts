import express from "express";

import { userRoute } from "./user.route";
import { authRoute } from "./auth.route";
import { videoRoute } from "./video.route";

const router = express.Router();

const mainRoutes = [
  {
    path: "/users",
    route: userRoute,
  },
  {
    path: "/auth",
    route: authRoute,
  },
  {
    path: "/videos",
    route: videoRoute,
  },
];

mainRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

export { router as routes };
