import express from "express";

import { authController } from "../controllers";
import validate from "../middlewares/validate";
import { authValidation } from "../validations";

const router = express.Router();

router.route("/register").post(validate(authValidation.register), authController.register);

router.route("/login").post(validate(authValidation.login), authController.login);

export { router as authRoute };
