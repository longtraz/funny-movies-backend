export const PORT = process.env.PORT || 8080;
export const IS_PRODUCTION = process.env.NODE_ENV === "production";
export const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || "funny-movies-jwt-secret-key";
export const AUTHENTICATED_USER_TOKEN_TTL = process.env.AUTHENTICATED_USER_TOKEN_TTL || 86400;
