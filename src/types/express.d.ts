import { PublicUser } from ".";

declare module "express" {
  export interface Request {
    requester?: PublicUser;
  }
}
