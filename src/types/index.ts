import { User } from "@prisma/client";

export type PublicUser = Omit<User, "password">;

export type SortOrder = "asc" | "desc";
