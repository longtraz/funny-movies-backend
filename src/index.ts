import { Server } from "http";

import { app } from "./app";
import { PORT } from "./configs";
import { prisma } from "./client";
import { logger } from "./libs/logger";

let server: Server;

logger.info("Server is starting...");

prisma.$connect().then(() => {
  logger.info("Connected to SQL Database");
  server = app.listen(PORT, () => {
    logger.info(`Listening to port ${PORT}`);
  });
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info("Server closed");
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error: unknown) => {
  logger.error(error);

  exitHandler();
};

process.on("uncaughtException", unexpectedErrorHandler);
process.on("unhandledRejection", unexpectedErrorHandler);

process.on("SIGTERM", () => {
  logger.info("SIGTERM received");

  if (server) server.close();
});
