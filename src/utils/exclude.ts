import { cloneDeep } from "lodash";

export const exclude = <Type, Key extends keyof Type>(obj: Type, keys: Key[]): Omit<Type, Key> => {
  const newObj = cloneDeep(obj);

  for (const key of keys) {
    delete newObj[key];
  }

  return newObj;
};
