import httpStatus from "http-status";

import { prisma } from "../client";
import { ApiError } from "../errors/ApiError";
import { DEFAULT_LIMIT, DEFAULT_OFFSET } from "../constants";
import { SortOrder } from "../types";

export const isValidYoutubeURL = (url: string) => {
  return new RegExp("^(https?://)?((www.)?youtube.com|youtu.be)/.+$").test(url);
};

export const createYoutubeVideo = async (url: string, lastUpdatedBy: number) => {
  if (!isValidYoutubeURL(url)) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid Youtube URL");

  const createdVideo = await prisma.youtubeVideo.create({
    data: { url, lastUpdatedBy },
    include: {
      lastUpdatedByUser: true,
    },
  });

  return createdVideo;
};

export const listVideos = async (
  offset = DEFAULT_OFFSET,
  limit = DEFAULT_LIMIT,
  sortBy = "createdAt",
  sortOrder: SortOrder = "desc"
) => {
  const videos = await prisma.youtubeVideo.findMany({
    skip: offset,
    take: limit,
    orderBy: { [sortBy]: sortOrder },
    include: {
      lastUpdatedByUser: true,
    },
  });

  return videos;
};

export const countVideos = () => {
  return prisma.youtubeVideo.count();
};
