import httpStatus from "http-status";
import {
  JsonWebTokenError,
  NotBeforeError,
  sign as signJWT,
  TokenExpiredError,
  verify as verifyJWT,
} from "jsonwebtoken";

import { doesPasswordMatch } from "../libs/password";
import * as userService from "./user.service";
import { ApiError } from "../errors/ApiError";
import { exclude } from "../utils/exclude";
import { PublicUser } from "../types";
import { AUTHENTICATED_USER_TOKEN_TTL, JWT_SECRET_KEY } from "../configs";

export const authenticate = async (email: string, password: string): Promise<PublicUser> => {
  const user = await userService.getUserByEmail(email);

  if (!user || !(await doesPasswordMatch(password, user.password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, "Incorrect email or password");
  }

  return exclude(user, ["password"]);
};

export const generateToken = (user: PublicUser) => {
  return signJWT(user, JWT_SECRET_KEY, { expiresIn: AUTHENTICATED_USER_TOKEN_TTL });
};

export const verifyAuthToken = async (token: string): Promise<PublicUser | null> => {
  try {
    const decodedData = verifyJWT(token, JWT_SECRET_KEY) as PublicUser;

    if (!decodedData.email) return null;

    return decodedData;
  } catch (error) {
    if (error instanceof TokenExpiredError || error instanceof JsonWebTokenError || error instanceof NotBeforeError) {
      return null;
    }

    throw error;
  }
};
