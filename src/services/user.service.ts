import { User } from "@prisma/client";
import httpStatus from "http-status";

import { prisma } from "../client";
import { ApiError } from "../errors/ApiError";
import { encryptPassword } from "../libs/password";
import { PublicUser } from "../types";
import { exclude } from "../utils/exclude";

export const createUser = async (email: string, password: string): Promise<PublicUser> => {
  const existingUesr = await getUserByEmail(email);

  if (existingUesr) throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");

  const createdUser = await prisma.user.create({
    data: {
      email,
      password: await encryptPassword(password),
    },
  });

  return normalizeUser(createdUser);
};

export const getUserById = async (id: number): Promise<PublicUser | null> => {
  const foundUser = await prisma.user.findUnique({
    where: { id },
  });

  if (!foundUser) return null;

  return normalizeUser(foundUser);
};

export const getUserByEmail = async (email: string): Promise<User | null> => {
  const foundUser = await prisma.user.findUnique({
    where: { email },
  });

  return foundUser;
};

export const normalizeUser = (user: User): PublicUser => {
  return exclude(user, ["password"]);
};
