import express from "express";
import cors from "cors";

import { routes } from "./routes";
import { errorConverter, errorHandler } from "./middlewares/error";

const app = express();

app.use(express.json());

// Parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// Enable CORS
app.use(cors());
app.options("*", cors());

// API routes
app.use("/api", routes);

// Handles errors and convert errors to ApiError, if needed
app.use(errorConverter);
app.use(errorHandler);

export { app };
