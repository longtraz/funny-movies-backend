import Joi from "joi";

export const createVideo = {
  body: Joi.object().keys({
    url: Joi.string().required(),
  }),
};

export const listVideos = {
  query: Joi.object().keys({
    offset: Joi.number().positive().optional(),
    limit: Joi.number().positive().optional(),
  }),
};
