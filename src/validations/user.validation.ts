import Joi from "joi";

export const createUser = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().min(6),
  }),
};

export const getUser = {
  params: Joi.object().keys({
    userId: Joi.number().integer(),
  }),
};
