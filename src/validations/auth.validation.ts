import Joi from "joi";

import { createUser } from "./user.validation";

export const register = createUser;

export const login = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required(),
  }),
};
