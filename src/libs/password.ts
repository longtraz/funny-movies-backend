import bcrypt from "bcryptjs";

const SALT_LENGTH = 8;

export const encryptPassword = async (password: string) => {
  const encryptedPassword = await bcrypt.hash(password, SALT_LENGTH);

  return encryptedPassword;
};

export const doesPasswordMatch = async (currentPassword: string, targetPassword: string) => {
  return bcrypt.compare(currentPassword, targetPassword);
};
