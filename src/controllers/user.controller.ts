import httpStatus from "http-status";

import { catchAsync } from "../libs/catchAsync";
import { userService } from "../services";
import { ApiError } from "../errors/ApiError";

export const createUser = catchAsync(async (req, res) => {
  const { email, password } = req.body;

  const user = await userService.createUser(email, password);

  res.status(httpStatus.CREATED).send(user);
});

export const getUser = catchAsync(async (req, res) => {
  const { userId } = req.params;
  const user = await userService.getUserById(userId);

  if (!user) throw new ApiError(httpStatus.NOT_FOUND, "User not found");

  res.send(user);
});
