export * as authController from "./auth.controller";
export * as userController from "./user.controller";
export * as videoController from "./video.controller";
