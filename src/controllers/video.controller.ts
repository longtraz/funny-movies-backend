import httpStatus from "http-status";
import { Request } from "express";

import { catchAsync } from "../libs/catchAsync";
import { videoService } from "../services";

export const createVideo = catchAsync(async (req, res) => {
  const { url } = req.body;

  const video = await videoService.createYoutubeVideo(url, (req as Request).requester!.id);

  res.status(httpStatus.CREATED).send(video);
});

export const listVideos = catchAsync(async (req, res) => {
  const { offset, limit } = req.query;

  const [videos, total] = await Promise.all([
    videoService.listVideos(Number(offset), Number(limit)),
    videoService.countVideos(),
  ]);

  res.send({ list: videos, total });
});
