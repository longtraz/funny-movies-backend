import httpStatus from "http-status";

import { catchAsync } from "../libs/catchAsync";
import { authService, userService } from "../services";
import { generateToken } from "../services/auth.service";

export const register = catchAsync(async (req, res) => {
  const { email, password } = req.body;

  const user = await userService.createUser(email, password);

  const token = generateToken(user);

  res.status(httpStatus.CREATED).send({ user, token });
});

export const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;

  const user = await authService.authenticate(email, password);

  const token = generateToken(user);

  res.send({ user, token });
});
