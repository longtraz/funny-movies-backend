export default {
  projects: [
    {
      displayName: "unit",
      preset: "ts-jest",
      testEnvironment: "node",
      clearMocks: true,
      moduleNameMapper: {
        "^@/(.*)": "<rootDir>/src/$1",
      },
      transform: {
        ".(ts|tsx)": "ts-jest",
      },
      globals: {
        "ts-jest": {
          compiler: "ttypescript",
          tsconfig: "tsconfig.test.json",
        },
      },
      setupFiles: ["<rootDir>/test/unit/jest-ts-auto-mock.config.ts"],
      testMatch: ["<rootDir>/test/unit/**/*.spec.ts"],
    },
  ],
};
