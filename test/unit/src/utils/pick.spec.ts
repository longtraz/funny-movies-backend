import { pick } from "../../../../src/utils/pick";

describe("pick function", () => {
  const testObj = {
    id: 1,
    name: "John",
    age: 30,
    address: {
      street: "123 Main St",
      city: "Anytown",
      state: "CA",
    },
  };

  it("should pick a single key from the object", () => {
    const picked = pick(testObj, ["name"]);

    expect(picked).toEqual({ name: "John" });
  });

  it("should pick multiple keys from the object", () => {
    const picked = pick(testObj, ["id", "age"]);

    expect(picked).toEqual({ id: 1, age: 30 });
  });

  it("should ignore keys that are not in the object", () => {
    const picked = pick(testObj, ["id", "missingKey"]);

    expect(picked).toEqual({ id: 1 });
  });

  it("should handle nested objects", () => {
    const picked = pick(testObj, ["address"]);

    expect(picked).toEqual({ address: { street: "123 Main St", city: "Anytown", state: "CA" } });
  });

  it("should return an empty object when keys array is empty", () => {
    const picked = pick(testObj, []);

    expect(picked).toEqual({});
  });

  it("should not modify the original object", () => {
    pick(testObj, ["name"]);

    expect(testObj).toEqual({
      id: 1,
      name: "John",
      age: 30,
      address: {
        street: "123 Main St",
        city: "Anytown",
        state: "CA",
      },
    });
  });

  it("should return a new object instance", () => {
    const picked = pick(testObj, ["name"]);

    expect(picked).not.toBe(testObj);
  });
});
