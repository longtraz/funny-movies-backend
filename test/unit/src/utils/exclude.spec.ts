import { exclude } from "../../../../src/utils/exclude";

describe("exclude function", () => {
  const testObject = {
    id: 1,
    name: "John",
    age: 30,
  };

  it("should exclude a single key from the object", () => {
    const excluded = exclude(testObject, ["name"]);

    expect(excluded).toEqual({ id: 1, age: 30 });
  });

  it("should exclude multiple keys from the object", () => {
    const excluded = exclude(testObject, ["name", "age"]);

    expect(excluded).toEqual({ id: 1 });
  });

  it("should not modify the original object", () => {
    exclude(testObject, ["name"]);

    expect(testObject).toEqual({ id: 1, name: "John", age: 30 });
  });

  it("should return a new object instance", () => {
    const excluded = exclude(testObject, ["name"]);

    expect(excluded).not.toBe(testObject);
  });
});
